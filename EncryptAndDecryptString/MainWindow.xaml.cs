﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EncryptAndDecryptString
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            // The string to be encrypted
            string originalString = SearchTextBox.Text;
            if (string.IsNullOrEmpty(originalString) || string.IsNullOrWhiteSpace(originalString)) 
            {
                MessageBox.Show("Please Enter the string for Encrypting and Decrypting","Error Message",MessageBoxButton.OK,MessageBoxImage.Error);
                return;
            }

            // Generate a random key and IV
            byte[] key = new byte[32];
            byte[] iv = new byte[16];
            var rng = new RNGCryptoServiceProvider();
            rng.GetBytes(key);
            rng.GetBytes(iv);
            

            // Encrypt the string
            string encryptedString = EncryptString(originalString, key, iv);

            // Decrypt the string
            string decryptedString = DecryptString(encryptedString, key, iv);

            // Print the results
            EncryptTextBox.Text = encryptedString;
            DecryptTextBox.Text = decryptedString;
        }

        static string EncryptString(string plainText, byte[] key, byte[] iv)
        {
            byte[] encrypted;

            using (Aes aes = Aes.Create())
            {
                aes.Key = key;
                aes.IV = iv;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (var ms = new System.IO.MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                    {
                        using (var sw = new System.IO.StreamWriter(cs))
                        {
                            sw.Write(plainText);
                        }

                        encrypted = ms.ToArray();
                    }
                }
            }

            return Convert.ToBase64String(encrypted);
        }

        static string DecryptString(string cipherText, byte[] key, byte[] iv)
        {
            string plaintext = null;

            using (Aes aes = Aes.Create())
            {
                aes.Key = key;
                aes.IV = iv;

                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                byte[] cipherBytes = Convert.FromBase64String(cipherText);

                using (var ms = new System.IO.MemoryStream(cipherBytes))
                {
                    using (var cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                    {
                        using (var sr = new System.IO.StreamReader(cs))
                        {
                            plaintext = sr.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;
        }       
    }
}
